﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrostGateTest
{
    [Serializable]
    public class CharacterData
    {
        public enum CharacterTypes
        {
            players,
            enemies,
        }
        
        public string name = "enemie";

        public CharacterTypes type = CharacterTypes.enemies;
        
        public Transform prefub;
        
        public float attack = 1;
        
        public float defence = 0.5f;
        
        public float hp = 10;
        
        public float speed = 5;
        
        public bool canShoot = false;
        
        public bool canAbility = false;

    }
}