﻿using System.Collections;
using System.Collections.Generic;
using ProBuilder2.Common;
using UnityEditor;
using UnityEngine;

namespace FrostGateTest
{
	public class GameDataWindow : EditorWindow
	{
		private static EditorWindow thisWindow;
		private int viewIndex = 0;

		[MenuItem("FrostGateTest/GameData")]
		static void Init()
		{
			thisWindow = EditorWindow.GetWindow(typeof(GameDataWindow));
			thisWindow.Show();
		}

		void  OnEnable () {

		}

		void OnGUI()
		{
			EditorUtility.SetDirty(GameData.GetInstance());

			if (GUILayout.Button("СОХРАНИТЬ"))
			{
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}
			GUILayout.Space(20);
			
			EditorGUILayout.LabelField("ГЛОБАЛЬНЫЕ НАСТРОЙКИ");
			
			GameData.GetInstance().critChance =
				EditorGUILayout.FloatField("Шанс крита:", GameData.GetInstance().critChance);

			GameData.GetInstance().shootDistance =
				EditorGUILayout.FloatField("Дистанция стрелбы:", GameData.GetInstance().shootDistance);

			GameData.GetInstance().abilityCost =
				EditorGUILayout.FloatField("Цена абилки:", GameData.GetInstance().abilityCost);

			GameData.GetInstance().abilityDuration =
				EditorGUILayout.FloatField("Длительность абилки:", GameData.GetInstance().abilityDuration);



			GUILayout.Space(20);

			EditorGUILayout.LabelField("ПЕРСОНАЖИ");

			GUILayout.BeginHorizontal();
			
			if (GUILayout.Button("СОЗДАТЬ"))
			{
				GameData.GetInstance().allCharacters =
					GameData.GetInstance().allCharacters.Add(new CharacterData());
				viewIndex = GameData.GetInstance().allCharacters.Length - 1;
				return;
			}

			if (GameData.GetInstance().allCharacters.Length > 0)
			{
				
				
				if (GUILayout.Button("УДАЛИТЬ"))
				{
					GameData.GetInstance().allCharacters =
						GameData.GetInstance().allCharacters.RemoveAt(viewIndex);
					
					if (viewIndex > GameData.GetInstance().allCharacters.Length - 1)
					{
						viewIndex = 0;
						return;
					}				
					
					return;
				}
			}
			
			GUILayout.EndHorizontal();

			if (GameData.GetInstance().allCharacters.Length == 0)
			{
				return;
			}

			GUILayout.Space(20);

			EditorGUILayout.LabelField("Номер персонажа - " + viewIndex);
			
			GUILayout.Space(20);

			GameData.GetInstance().allCharacters[viewIndex].name =
				EditorGUILayout.TextField("Имя:", GameData.GetInstance().allCharacters[viewIndex].name);
			
			GameData.GetInstance().allCharacters[viewIndex].prefub =
				EditorGUILayout.ObjectField("Префаб:", GameData.GetInstance().allCharacters[viewIndex].prefub, typeof(Transform), true) as Transform;


			GameData.GetInstance().allCharacters[viewIndex].type =
				(CharacterData.CharacterTypes)EditorGUILayout.EnumPopup("Тип персонажа:", GameData.GetInstance().allCharacters[viewIndex].type);


			
			GameData.GetInstance().allCharacters[viewIndex].attack =
				EditorGUILayout.FloatField("Атака:", GameData.GetInstance().allCharacters[viewIndex].attack);
			
			GameData.GetInstance().allCharacters[viewIndex].defence =
				EditorGUILayout.FloatField("Защита:", GameData.GetInstance().allCharacters[viewIndex].defence);
			
			GameData.GetInstance().allCharacters[viewIndex].hp =
				EditorGUILayout.FloatField("Здоровье:", GameData.GetInstance().allCharacters[viewIndex].hp);
			
			GameData.GetInstance().allCharacters[viewIndex].speed =
				EditorGUILayout.FloatField("Скорость:", GameData.GetInstance().allCharacters[viewIndex].speed);			
			
			GameData.GetInstance().allCharacters[viewIndex].canShoot =
				EditorGUILayout.Toggle("Стреляющий:", GameData.GetInstance().allCharacters[viewIndex].canShoot);
			
			GameData.GetInstance().allCharacters[viewIndex].canAbility =
				EditorGUILayout.Toggle("С абилкой:", GameData.GetInstance().allCharacters[viewIndex].canAbility);

			GUILayout.Space(20);
			
			GUILayout.BeginHorizontal();
			
			if (GUILayout.Button("Предыдущий"))
			{
				viewIndex--;
				if (viewIndex < 0)
					viewIndex = GameData.GetInstance().allCharacters.Length - 1;
				return;
			}
			
			if (GUILayout.Button("Следующий"))
			{
				viewIndex++;
				if (viewIndex > GameData.GetInstance().allCharacters.Length - 1)
					viewIndex = 0;
				return;
			}
			
			GUILayout.EndHorizontal();
		}
		
	}
}