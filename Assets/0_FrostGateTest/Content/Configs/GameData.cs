﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrostGateTest
{
	[CreateAssetMenu(menuName = "FGT/GameData")]
	public class GameData : ScriptableObject
	{

		public float critChance = 0.2f;
		
		public float shootDistance = 10f;
		
		public float abilityDuration = 10f;
		
		public float abilityCost = 10f;

		
		public CharacterData[] allCharacters = new CharacterData[0];


		private static GameData instance = null;

		public static GameData GetInstance()
		{
			if (instance == null)
			{
				GameData[] all = Resources.LoadAll<GameData>("");
				
				if(all != null && all.Length > 0)
					instance = all[0];
								
				if(instance == null)
					Debug.LogError("GameData asset not exist");
			}
			
			return instance;
		}

	}
}