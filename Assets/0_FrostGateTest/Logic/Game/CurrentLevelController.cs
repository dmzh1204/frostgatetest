﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrostGateTest
{

	public class CurrentLevelController : MonoBehaviour
	{

		private static bool isLevelCompleted = false;

		public static bool IsLevelCompleted => isLevelCompleted;

		private static Vector3 finishPosition;

		public static Vector3 FinishPosition => finishPosition;

		public static int victoriesCount = 0;

		public static int VictoriesCount => victoriesCount;

		[SerializeField] private Transform finishPointTransform = null;

		public static event Action OnLevelStarted;
		public static event Action<bool> OnLevelCompleted;

		// Use this for initialization
		void Start()
		{

			Init();

			Invoke("StartGame", 1);
			CharacterController.OnDeath += OnCharacterDeath;
		}

		private void OnDestroy()
		{
			CharacterController.OnDeath -= OnCharacterDeath;
		}

		void Init()
		{
			finishPosition = finishPointTransform.position;
		}

		void StartGame()
		{
			isLevelCompleted = false;
			OnLevelStarted?.Invoke();
		}

		public static void CompletLevel(bool result)
		{
			if (isLevelCompleted)
				return;

			if (result)
			{
				victoriesCount++;
			}
			else
			{
				victoriesCount = 0;
			}

			isLevelCompleted = true;
			OnLevelCompleted?.Invoke(result);
		}

		private void OnCharacterDeath(CharacterController characterController)
		{
			bool isAnyPlayerLive = false;
			foreach (var character in CharacterController.AllCharacters)
			{
				if (character.CharacterData.type == CharacterData.CharacterTypes.players
				    && character.CurrentHealth > 0)
				{
					isAnyPlayerLive = true;
					break;
				}
			}

			if (isAnyPlayerLive == false)
			{
				CompletLevel(false);
			}
		}
	}
}