﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FrostGateTest
{
	public class UIController : MonoBehaviour
	{

		[SerializeField] private GameObject victoryPanel = null;
		[SerializeField] private GameObject failPanel = null;
		[SerializeField] private Text currentLevelText = null;
		
		// Use this for initialization
		void Start()
		{
			CurrentLevelController.OnLevelCompleted += OnLevelCompleted;
			victoryPanel.SetActive(false);
			failPanel.SetActive(false);
			currentLevelText.text = "Уровень: " + (CurrentLevelController.victoriesCount + 1);
		}

		private void OnLevelCompleted(bool result)
		{
			victoryPanel.SetActive(result);
			failPanel.SetActive(!result);
		}

		private void OnDestroy()
		{
			CurrentLevelController.OnLevelCompleted -= OnLevelCompleted;
		}
	}
}