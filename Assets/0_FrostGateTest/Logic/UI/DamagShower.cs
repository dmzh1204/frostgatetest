﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FrostGateTest
{

	public class DamagShower : MonoBehaviour
	{
		[SerializeField] private RectTransform prefub;
		
		// Use this for initialization
		void Start()
		{
			CharacterController.OnDamage += OnDamage;
		}

		void OnDestroy()
		{
			CharacterController.OnDamage -= OnDamage;
		}

		void OnDamage(Vector3 position, float value)
		{
			RectTransform rt = (RectTransform)Instantiate(prefub, prefub.parent);
			rt.gameObject.SetActive(true);
			rt.GetComponent<Text>().text = "-" + value;
			rt.position = Camera.main.WorldToScreenPoint(position);
			Destroy(rt.gameObject, 0.5f);
		}
	}
}
