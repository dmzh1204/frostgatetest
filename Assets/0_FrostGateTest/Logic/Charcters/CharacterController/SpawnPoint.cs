﻿using System.Collections;
using System.Collections.Generic;
using FrostGateTest;
using UnityEngine;
using UnityEngine.Serialization;
using CharacterController = FrostGateTest.CharacterController;

namespace FrostGateTest
{

	public class SpawnPoint : MonoBehaviour
	{

		[SerializeField] private CharacterData.CharacterTypes characterType = CharacterData.CharacterTypes.enemies;

		void Start()
		{
			CurrentLevelController.OnLevelStarted += OnLevelStarted;
		}

		void OnDestroy()
		{
			CurrentLevelController.OnLevelStarted -= OnLevelStarted;
		}
		
		void OnLevelStarted()
		{
			List<CharacterData> allPosibleCharacters = new List<CharacterData>();

			foreach (var character in GameData.GetInstance().allCharacters)
			{
				if (character.type == characterType)
				{
					allPosibleCharacters.Add(character);
				}
			}
			
			if (characterType == CharacterData.CharacterTypes.players)
			{
				

				for (int i = 0; i < allPosibleCharacters.Count; i++)
				{
					Vector3 pos = transform.position;

					if (i > 0)
					{
						pos += Quaternion.AngleAxis(90 * i, Vector3.up) * transform.forward * 5;
					}
					
					Spawn(allPosibleCharacters[i], pos, transform.rotation);
				}

				Destroy(gameObject);
			}

			if (characterType == CharacterData.CharacterTypes.enemies)
			{
				Spawn(allPosibleCharacters[Random.Range(0, allPosibleCharacters.Count)], transform.position, transform.rotation);
				Destroy(gameObject);
			}
		}

		private void Spawn(CharacterData data, Vector3 position, Quaternion rotation)
		{
			Transform newCharacterTransform = (Transform) Instantiate(data.prefub, position, rotation);

			if (data.type == CharacterData.CharacterTypes.players)
			{
				newCharacterTransform.gameObject.tag = "Player";
			}

			float levelMultiplier = 1;

			if (data.type == CharacterData.CharacterTypes.enemies)
			{
				levelMultiplier += 0.2f * CurrentLevelController.VictoriesCount;
			}
			
			newCharacterTransform.gameObject.AddComponent<CharacterController>().Init(data, levelMultiplier);
		}
	}
}