﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace FrostGateTest
{
	
	public class CharacterController : MonoBehaviour
	{
		private static List<CharacterController> allCharacters = new List<CharacterController>();

		public static CharacterController[] AllCharacters => allCharacters.ToArray();

		public static event Action<Vector3, float> OnDamage;
		public static event Action<CharacterController> OnDeath;
		
		private Rigidbody rigidbody = null;
		
		private Animator animator = null;
		
		private CapsuleCollider collider = null;
		
		private CharacterData characterData = null;

		private float levelMultiplier = 1;

		private CharacterBrain brain;
		
		public CharacterData CharacterData
		{
			get { return characterData; }
		}

		private float currentHealth = 0;

		public float CurrentHealth => currentHealth;

		private float currentPower = 0;

		private bool isAttackNow = false;

		private float nextPosibleAttackTime = 0;

		private const float attackInterval = 1;

		private Vector3 moveVector = Vector3.zero;

		public Vector3 MoveVector
		{
			get { return moveVector; }
			set
			{
				moveVector = value;
				moveVector.y = 0;
				moveVector = moveVector.normalized;
			}
		}

		private Vector3 lookVector = Vector3.forward;

		public Vector3 LookVector
		{
			get { return lookVector; }
			set
			{
				lookVector = value;
			}
		}

		float abilityCompletTime = 0;

		public void Init(CharacterData data, float levMultiplier = 1)
		{
			characterData = data;

			currentHealth = data.hp;

			currentPower = 0;

			nextPosibleAttackTime = Time.fixedTime;
			
			levelMultiplier = levMultiplier;
			brain = new CharacterBrain(this);
			rigidbody = gameObject.AddComponent<Rigidbody>();

			rigidbody.useGravity = false;

			rigidbody.freezeRotation = true;
			
			collider = gameObject.AddComponent<CapsuleCollider>();
			
			collider.center = Vector3.up;
			collider.radius = 0.5f;
			collider.height = 1;

			animator = GetComponent<Animator>();

			gameObject.AddComponent<AnimationEventHandler>();
			
			allCharacters.Add(this);
		}

		public float GetAttackDistance()
		{
			if (CharacterData.canShoot)
			{
				return GameData.GetInstance().shootDistance;
			}
			else
			{
				return 1;
			}
		}

		public void StartAttack()
		{
			isAttackNow = true;
		}

		public void StopAttack()
		{
			isAttackNow = false;
		}
		
		private void FixedUpdate()
		{
			AttackHandler();
			AnimatorHandler();
			PhysicHandler();
			
			brain.Update(Time.fixedDeltaTime);
		}

		void PhysicHandler()
		{
			rigidbody.velocity = moveVector * characterData.speed;

			Vector3 forwardVector = lookVector;
			forwardVector.y = 0;

			transform.forward = Vector3.RotateTowards(transform.forward, forwardVector.normalized, Time.fixedDeltaTime * 10,
				Time.fixedDeltaTime);
			
			RaycastHit rh;

			if (Physics.Raycast(transform.position + Vector3.up, Vector3.down, out rh, 5, 1))
			{
				transform.position = rh.point;
			}
		}

		void AnimatorHandler()
		{
			Vector3 localMove = transform.InverseTransformDirection(moveVector);
			
			animator.SetFloat("forward", localMove.z);
			animator.SetFloat("strafe", localMove.x);
		}

		void AttackHandler()
		{
			if(isAttackNow == false)
				return;
			
			if(nextPosibleAttackTime > Time.fixedTime)
				return;

			nextPosibleAttackTime = Time.fixedTime + GetAttackInterval();

			RaycastHit rh;

			if (characterData.canShoot)
			{
				animator.SetTrigger("shoot");
			}
			else
			{
				animator.SetTrigger("kick");
			}

			Vector3 stratAttackPos = transform.position + Vector3.up;
			
			Vector3 endAttackPos = stratAttackPos + lookVector.normalized * GetAttackDistance();

			bool isCrit = false;
			
			if (Physics.Raycast(stratAttackPos, lookVector, out rh, GetAttackDistance(), 1))
			{
				endAttackPos = rh.point;
				
				CharacterController cc = rh.transform.GetComponent<CharacterController>();

				if (cc == null)
				{
					
					return;
				}
				
				if (cc.characterData.type != characterData.type)
				{
					AddPower();
					float damag = characterData.attack * levelMultiplier;
					
					if (GameData.GetInstance().critChance > Random.value)
					{
						isCrit = true;
						damag *= 3;
					}
					
					cc.Damage(damag);
				}
			}

			if (IsSpeedShootAbility())
			{
				EffectUtilities.DrawLine(stratAttackPos, endAttackPos, Color.cyan, 0.1f, 0.3f);
				return;
			}

			if (isCrit)
			{
				EffectUtilities.DrawLine(stratAttackPos, endAttackPos, Color.red, 0.2f, 0.2f);
			}
			else
			{
				EffectUtilities.DrawLine(stratAttackPos, endAttackPos, Color.yellow);
			}
		}

		public void Damage(float damagValue)
		{
			AddPower();

			float damag = damagValue - characterData.defence;
			Debug.Log("Damag = " + damag);
			currentHealth -= damag;

			OnDamage?.Invoke(transform.position, damag);
			
			if (currentHealth <= 0)
			{
				OnDeath?.Invoke(this);
				
				animator.SetTrigger("death");

				Debug.Log("Death");
				
				Destroy(rigidbody);
				Destroy(collider);
				Destroy(this);
				
				Destroy(gameObject, 2);
			}
			else
			{
				animator.SetTrigger("damag");
			}
		}

		private float GetAttackInterval()
		{
			if (IsSpeedShootAbility())
			{
				return attackInterval / 4;
			}
			else
			{
				return attackInterval;
			}
		}

		private bool IsSpeedShootAbility()
		{
			if (characterData.canAbility && abilityCompletTime > Time.fixedTime)
				return true;

			return false;
		}
		
		private void AddPower()
		{
			if(characterData.canAbility == false)
				return;
			
			if (IsSpeedShootAbility())
				return;
				
			currentPower++;
			
			if (currentPower >= GameData.GetInstance().abilityCost)
			{
				currentPower = 0;
				abilityCompletTime = Time.fixedTime + GameData.GetInstance().abilityDuration;
			}
		}
		
		private void OnDestroy()
		{
			brain?.Breack();
			allCharacters.Remove(this);
		}

	}
}