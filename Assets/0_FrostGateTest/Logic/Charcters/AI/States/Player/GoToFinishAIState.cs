﻿using System.Collections;
using System.Collections.Generic;
using FrostGateTest;
using UnityEngine;
using CharacterController = UnityEngine.CharacterController;

namespace FrostGateTest
{
    public class GoToFinishAIState : CharacterBaseAIState
    {
        private float nextTimeSearch = 0;

        public GoToFinishAIState(CharacterBrain charBrain) : base(charBrain)
        {
            characterBrain.CharacterController.StopAttack();
            nextTimeSearch = Time.fixedTime + Random.value;
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            characterBrain.PathFinder.Update(characterBrain.CharacterController.transform.position,
                CurrentLevelController.FinishPosition);

            characterBrain.CharacterController.MoveVector = characterBrain.PathFinder.GetDirection();
            characterBrain.CharacterController.LookVector = characterBrain.PathFinder.GetDirection();

            SearchEnemies();
        }

        void SearchEnemies()
        {
            if (nextTimeSearch > Time.fixedTime)
                return;

            nextTimeSearch = Time.fixedTime + 1;

            CharacterController targetCharacter = AIUtilities.GetClosestEnemy(characterBrain.CharacterController,
                characterBrain.CharacterController.GetAttackDistance());


            if (targetCharacter != null)
            {
                characterBrain.CurrentAiState = new PlayerAttackEnemyAIState(characterBrain, targetCharacter);
            }
        }
    }
}