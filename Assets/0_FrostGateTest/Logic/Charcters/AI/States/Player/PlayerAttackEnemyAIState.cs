﻿using System.Collections;
using System.Collections.Generic;
using FrostGateTest;
using UnityEngine;

namespace FrostGateTest
{

	public class PlayerAttackEnemyAIState : CharacterBaseAIState
	{
		private const float interval = 1;

		private float nextTimeUpdate = 0;
		
		private CharacterController attackedEnemy = null;


		public PlayerAttackEnemyAIState(CharacterBrain charBrain, CharacterController enemy) : base(charBrain)
		{
			attackedEnemy = enemy;
		}

		public override void Update(float deltaTime)
		{
			base.Update(deltaTime);
			
			if (attackedEnemy == null)
			{
				characterBrain.CurrentAiState = new GoToFinishAIState(characterBrain);
				return;
			}
			
			if(nextTimeUpdate > Time.fixedTime)
				return;

			nextTimeUpdate = Time.fixedTime + interval;
			
			characterBrain.CharacterController.LookVector =
				attackedEnemy.transform.position - characterBrain.CharacterController.transform.position;

			float dist = Vector3.Distance(attackedEnemy.transform.position,
				characterBrain.CharacterController.transform.position);
			
			if (dist > characterBrain.CharacterController.GetAttackDistance())
			{
				characterBrain.CharacterController.StopAttack();
				
				characterBrain.CurrentAiState = new GoToFinishAIState(characterBrain);
			}
			else
			{
				if (dist < characterBrain.CharacterController.GetAttackDistance() / 2)
				{
					characterBrain.CharacterController.MoveVector =
						AIUtilities.GetMoveDirectionForAttack(characterBrain.CharacterController, attackedEnemy,
							-characterBrain.CharacterController.transform.forward);
				}
				else
				{
					characterBrain.CharacterController.MoveVector =
						AIUtilities.GetMoveDirectionForAttack(characterBrain.CharacterController, attackedEnemy,
							Vector3.zero);
				}
				characterBrain.CharacterController.StartAttack();
			}
			
		}

	}
}