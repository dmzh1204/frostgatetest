﻿using System.Collections;
using System.Collections.Generic;
using FrostGateTest;
using UnityEngine;

namespace FrostGateTest
{

	public abstract class CharacterBaseAIState
	{
		protected CharacterBrain characterBrain = null;

		public CharacterBaseAIState(CharacterBrain charBrain)
		{
			characterBrain = charBrain;
		}

		public virtual void Update(float deltaTime)
		{

		}

	}
}