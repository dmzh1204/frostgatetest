﻿using System.Collections;
using System.Collections.Generic;
using FrostGateTest;
using UnityEngine;

namespace FrostGateTest
{
    public class StayAIState : CharacterBaseAIState
    {
        public StayAIState(CharacterBrain charBrain) : base(charBrain)
        {
            characterBrain.CharacterController.MoveVector = Vector3.zero;
            characterBrain.CharacterController.StopAttack();

        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

        }
    }
}