﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FrostGateTest
{
public class EnemyAttackPlayerAiState : CharacterBaseAIState
{
	private CharacterController attackedPlayer = null;
	
	public EnemyAttackPlayerAiState(CharacterBrain charBrain, CharacterController attackPlayer) : base(charBrain)
	{
		attackedPlayer = attackPlayer;
	}

	public override void Update(float deltaTime)
	{
		base.Update(deltaTime);

		if (attackedPlayer == null)
		{
			characterBrain.CurrentAiState = new EnemyWaitPlayerAiState(characterBrain);
			return;
		}
		
		characterBrain.PathFinder.Update(characterBrain.CharacterController.transform.position, attackedPlayer.transform.position);

		characterBrain.CharacterController.LookVector =
			attackedPlayer.transform.position - characterBrain.CharacterController.transform.position;

		if (Vector3.Distance(attackedPlayer.transform.position,
			    characterBrain.CharacterController.transform.position) > characterBrain.CharacterController.GetAttackDistance()
		    )
		{
			characterBrain.CharacterController.MoveVector = characterBrain.PathFinder.GetDirection();
			characterBrain.CharacterController.StopAttack();
			return;
		}
		
		characterBrain.CharacterController.MoveVector = Vector3.zero;
		characterBrain.CharacterController.StartAttack();
	}
}
}