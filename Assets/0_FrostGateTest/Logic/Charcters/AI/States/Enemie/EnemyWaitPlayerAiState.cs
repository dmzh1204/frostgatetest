﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrostGateTest
{
    public class EnemyWaitPlayerAiState : CharacterBaseAIState
    {
        private float nextTimeSearch = 0;

        private const float searchRadius = 15;

        public EnemyWaitPlayerAiState(CharacterBrain charBrain) : base(charBrain)
        {
            nextTimeSearch = Time.fixedTime + Random.value;
            characterBrain.CharacterController.StopAttack();
            characterBrain.CharacterController.MoveVector = Vector3.zero;
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            SearchPlayer();
        }

        void SearchPlayer()
        {
            if (nextTimeSearch > Time.fixedTime)
                return;

            nextTimeSearch = Time.fixedTime + 1;

            CharacterController targetCharacter =
                AIUtilities.GetClosestEnemy(characterBrain.CharacterController, searchRadius);


            if (targetCharacter != null)
            {
                characterBrain.CurrentAiState = new EnemyAttackPlayerAiState(characterBrain, targetCharacter);
            }
        }
    }
}