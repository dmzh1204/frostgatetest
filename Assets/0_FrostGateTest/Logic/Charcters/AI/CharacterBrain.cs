﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrostGateTest
{

	public class CharacterBrain
	{
		private CharacterController characterController = null;
		
		public CharacterController CharacterController => characterController;

		private CharacterBaseAIState currentAiState = null;

		public CharacterBaseAIState CurrentAiState
		{
			get
			{
				return currentAiState;
			}
			set
			{
				currentAiState = value;
			}
		}

		private PathFinder pathFinder;

		public PathFinder PathFinder => pathFinder;

		public CharacterBrain(CharacterController charController)
		{
			characterController = charController;
		
			pathFinder = new PathFinder();
			
			if (characterController.CharacterData.type == CharacterData.CharacterTypes.players)
			{
				CurrentAiState = new GoToFinishAIState(this);
			}
			else
			{
				CurrentAiState = new EnemyWaitPlayerAiState(this);
			}

			CurrentLevelController.OnLevelCompleted += OnLevelCompleted;
		}

		public void Update(float deltaTime)
		{
			if (characterController.CharacterData.type == CharacterData.CharacterTypes.players)
			{
				if (CurrentLevelController.IsLevelCompleted == false
				&& Vector3.Distance(characterController.transform.position, CurrentLevelController.FinishPosition) < 5)
				{
					CurrentLevelController.CompletLevel(true);
				}

			}
			
			CurrentAiState?.Update(deltaTime);
		}

		private void OnLevelCompleted(bool result)
		{
			CurrentAiState = new StayAIState(this);
		}

		public void Breack()
		{
			CurrentLevelController.OnLevelCompleted -= OnLevelCompleted;
		}
	}
	
}