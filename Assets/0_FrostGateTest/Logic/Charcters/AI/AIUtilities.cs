﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrostGateTest
{

	public class AIUtilities
	{

		public static Vector3 GetMoveDirectionForAttack(CharacterController myCharacter, CharacterController targetCharacter, Vector3 defaultMove)
		{
			RaycastHit rh;

			Vector3 vectorToTarget = targetCharacter.transform.position - myCharacter.transform.position;

			if (Physics.Raycast(myCharacter.transform.position + Vector3.up
				, vectorToTarget.normalized, out rh, myCharacter.GetAttackDistance(), 1) 
			    && rh.transform != targetCharacter.transform)
			{

				if (Physics.Raycast(myCharacter.transform.position + Vector3.up, myCharacter.transform.right, 2, 1))
				{
					defaultMove = -myCharacter.transform.right;
				}
				else
				{
					defaultMove = myCharacter.transform.right;
				}
			}
			
			return defaultMove;
		}

		public static CharacterController GetClosestEnemy(CharacterController myCharacter, float radius)
		{
			Collider[] colliders =
				Physics.OverlapSphere(myCharacter.transform.position, radius);

			float closestDist = float.MaxValue;

			CharacterController targetCharacter = null;
			
			foreach (var c in colliders)
			{
				CharacterController cc = c.GetComponent<CharacterController>();

				if (cc == null || cc == myCharacter)
					continue;

				if (cc.CharacterData.type != myCharacter.CharacterData.type)
				{
					float dist = Vector3.Distance(myCharacter.transform.position, cc.transform.position);

					if (dist < closestDist)
					{
						closestDist = dist;
						targetCharacter = cc;
					}
				}
			}
			
			return targetCharacter;
		}

	}
	
}